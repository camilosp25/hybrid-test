var accounts = [
    {
        userId: 1, accounts: [
            {number: 1234, type: 'Ahorros', amount: 100000},
            {number: 4567, type: 'Ahorros', amount: 200000}
        ]
    },
    {
        userId: 2, accounts: [
            {number: 5678, type: 'Ahorros', amount: 300000},
            {number: 7890, type: 'Corriente', amount: 400000}]
    },
    {
        userId: 3, accounts: [
            {number: 3456, type: 'Ahorros', amount: 300000},
            {number: 6987, type: 'Corriente', amount: 670000},
            {number: 1542, type: 'Corriente', amount: 600000}
        ]
    },
    {
        userId: 4, accounts: [
            {number: 3851, type: 'Ahorros', amount: 360000},
            {number: 3473, type: 'Corriente', amount: 870000},
            {number: 2353, type: 'Ahorros', amount: 9000000},
            {number: 1207, type: 'Corriente', amount: 5600000}
        ]
    },
]

$(document).on('pagebeforeshow', '#accounts', function () {
    $('#accounts-list').empty();
    var userId = sessionStorage.getItem('userId');
    var accountList = getAccounts(userId)[0].accounts;
    var item = {};
    var htmlItem = '';
    for (var i = 0; i < accountList.length; i++) {
        item = accountList[i];
        htmlItem += '<li class="account-item"><h2>' + item.number + '</h2>';
        htmlItem += '<p>Cuenta ' + item.type + '</p>';
        htmlItem += '<p>Saldo disponible  ' + item.amount + '</p></li>';

    }
    $('#accounts-list').append(htmlItem);
    $('#accounts-list').listview();
});

function getAccounts(userId){
    return accounts.filter(function (data){
       return data.userId = userId;
    });
}
