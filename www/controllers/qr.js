$(document).on('pagebeforeshow', '#qr', function () {
    $("#scanQR").click(function () {
        var errorPermission = function () {
            console.error('error permission');
        }
        if (hasPermission(cordova.plugins.permissions.CAMERA)) {
            scanQR();
        } else {
            requestPermission(cordova.plugins.permissions.CAMERA, function () {
                scanQR();
            }, errorPermission);
        }
    });

});

function scanQR() {
    var params = {
        'prompt_message': 'Leer QR', // Change the info message. A blank message ('') will show a default message
        'camera_id': 0, // Choose the camera source
        'beep_enabled': true, // Enables a beep after the scan
        'scan_type': 'normal', // Types of scan mode: normal = default black with white background / inverted = white bars on dark background / mixed = normal and inverted modes
        'barcode_formats': [
            'QR_CODE',
            'CODE_39',
            'CODE_128'], // Put a list of formats that the scanner will find. A blank list ([]) will enable scan of all barcode types
        'extras': {} // Additional extra parameters. See [ZXing Journey Apps][1] IntentIntegrator and Intents for more details
    }
    cordova.plugins.qr.scan(params, function (response) {
            console.log('success');
            $('#dataQR').empty();
            var data = JSON.parse(response);
            var html = '<div className="ui-grid-a">' +
                '<div className="ui-body ui-body-d">' +
                '    <h3>Nombre Destinatario</h3>' +
                '    <p>' + data.name + '</p>' +
                '</div>' +
                '<div className="ui-body ui-body-d">' +
                '    <h3>Cuenta Destinatario</h3>' +
                '    <p>' + data.account + '</p>' +
                '</div>' +
                '<div className="ui-body ui-body-d">' +
                '    <h3>Tipo de cuenta</h3>' +
                '    <p>' + data.accountType + '</p>' +
                '</div>' +
                '<div className="ui-body ui-body-d">' +
                '<h3>Monto</h3>' +
                '   <p>' + data.ammount + '</p>' +
                '</div>' +
                '</div>';
            $('#dataQR').html(html);
        },
        function (response) {
            console.log('error');
            console.log(response);
        });
}
