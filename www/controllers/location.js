$(document).on('pagebeforeshow', '#location', function () {
    navigator.geolocation.getCurrentPosition(function (position) {
            var latitude = position.coords.latitude
            var longitude = position.coords.longitude
            console.log(latitude);
            console.log(longitude);
            getMap(latitude, longitude);
        },
        function () {
            console.error('error location');
        }, {
            enableHighAccuracy: true
        });

});

function getMap(latitude, longitude) {

    var mapOptions = {
        center: new google.maps.LatLng(0, 0),
        zoom: 1,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map
    (document.getElementById("map"), mapOptions);


    var latLong = new google.maps.LatLng(latitude, longitude);

    var marker = new google.maps.Marker({
        position: latLong
    });

    marker.setMap(map);
    map.setZoom(15);
    map.setCenter(marker.getPosition());
}
