$(document).on('pagebeforeshow', '#contacts', function () {

    $('#contacts-list').empty();
    var errorPermission = function () {
        console.error('error permission');
    }

    if (cordova.platformId.toLowerCase() === 'android') {
        if (hasPermission(cordova.plugins.permissions.READ_CONTACTS)) {
            getContacts();
        } else {
            requestPermission(cordova.plugins.permissions.READ_CONTACTS, function(){
                console.log('permission granted');
                getContacts()
            }, errorPermission);
        }
    }
})

function getContacts() {
    phonecontacts.getContacts('', function (response) {
        console.log(response);
        var item = {};
        var htmlItem = '';
        for (var i = 0; i < response.length; i++) {
            item = JSON.parse(response[i]);
            htmlItem = '<li class="account-item">' +
                '<p><h2>' + item.name + '</h2></p>';
            htmlItem += '<p>' + item.mobileNumber + '</p></li>';
            $('#contacts-list').append(htmlItem);
        }
        $('#contacts-list').listview();
    }, function () {
        console.log('error getContacts');
    });
}
