var allowedUsers = [
    {userId: 1, username: 'csanchez', password: '123'},
    {userId: 2, username: 'achalarca', password: '123'},
    {userId: 3, username: 'apacheco', password: '123'},
    {userId: 4, username: 'todo1user', password: '123'},
]

$(document).on('pagebeforeshow', '#login', function () {
    if (localStorage.getItem('rememberme')) {
        $('#rememberme').prop('checked', true).checkboxradio('refresh');
        $("#username").val(localStorage.getItem('username'));
        $("#password").val(localStorage.getItem('password'));

        FingerprintAuth.isAvailable(function () {
            console.info('touchid available');
            $('#fingerPrintLogin').show();
        }, function () {
            console.warn('touchid not available');
            $('#fingerPrintLogin').hide();
        });
    }
    $("#btn_login").click(function () {
        var username = $("#username").val();
        var password = $("#password").val();
        validateUser(username, password);
    });
    $("#fingerPrintBtn").click(function () {
        var username = '';
        if (localStorage.getItem('rememberme')) {
            username = localStorage.getItem('username');
            var token = localStorage.getItem('token');
            decrypt(username, token)
        } else {
            username = $("#username").val();
            var password = $("#password").val();
            encrypt(username, password);
        }
    });
    $("#rememberme").click(function () {
        if ($("#rememberme").is(':checked')) {
            FingerprintAuth.isAvailable(function () {
                console.info('touchid available');
                $('#fingerPrintLogin').show();
            }, function () {
                console.warn('touchid not available');
                $('#fingerPrintLogin').hide();
            });
        }
    });
});

function validateUser(username, password, tokenBiometric) {
    var credentials = allowedUsers.filter(function (data) {
        return data.username === username && data.password === password;
    });

    if (credentials.length > 0) {
        sessionStorage.setItem('userId', credentials[0].userId);
        if ($("#rememberme").is(':checked')) {
            localStorage.setItem('rememberme', true);
            localStorage.setItem('username', username);
            localStorage.setItem('password', password);
            if (tokenBiometric) {
                localStorage.setItem('token', tokenBiometric);
            }
        } else {
            localStorage.removeItem('rememberme');
            localStorage.removeItem('username');
            localStorage.removeItem('password');
            localStorage.removeItem('token');
            $('#fingerPrintLogin').hide();
        }
        $.mobile.changePage("#accounts", {transition: "slide"});
    } else {
        $( "#popupDialog" ).popup( "open");
    }
}
