function hasPermission(permission) {
    cordova.plugins.permissions.checkPermission(permission, function (status) {
        if (status.hasPermission) {
            return true;
        } else {
            return false;
        }
    });
}

function requestPermission(permission, successCallback, errorCallback) {
    cordova.plugins.permissions.requestPermission(permission, successCallback, errorCallback);
}
