function showAlertButton(message) {
    hideAlert();
    $(document).scrollTop(0);
    var height = $(window).height(),
        width = $(window).width();
    $('#ui-alert-button').remove();
    $('<div id="ui-alert-bg"></div>').appendTo('body').show();
    $('body').append(
        '<div id="ui-alert-content"><div class="alertMssg"><p id="ui-alert-message">' +
        message +
        '</p></div><p id="ui-alert-button" style="display: inline-block;"></p></div>'
    );
    $('#ui-alert-button')
        .append(
            '<a href="#" data-role="button" data-corners="false"  id="btn_msg"  name="btn_msg" class="btnMssg">Aceptar</a>'
        )
        .trigger('create');
    $('#btn_msg')
        .unbind()
        .bind('click', function () {
            $('#ui-alert-content').removeClass('ui-alert-in');
            hideAlert();
        });

    var msgHeight = $('#ui-alert-content')[0].clientHeight;
    $('#ui-alert-content').css('z-index', '2147483644');
    $('#ui-alert-content')
        .css({
            top: (height - msgHeight) / 2,
            left: width / 2 - 280 / 2,
        })
        .addClass('ui-alert-in')
        .show();
    document.activeElement.blur();
    document.addEventListener('touchmove', function (e) {
        e.preventDefault();
    });
}

function hideAlert() {
    $('#ui-alert-content').addClass('ui-alert-out');
    $('#ui-alert-content').remove();
    $('#ui-alert-content').removeClass('ui-alert-out').remove();

    $('#ui-alert-bg').remove();
    $('#mapBackBtn').removeClass('ui-disabled');
    document.removeEventListener('touchmove', function (e) {}, false);
}
