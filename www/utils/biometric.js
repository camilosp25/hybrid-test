var appName = 'com.todo1.test';

function encrypt(username, password) {
    var encryptConfig = {
        clientId: appName,
        username: username,
        password: password
    };

    var successCallback = function (result) {
        console.log("successCallback(): " + JSON.stringify(result));
        if (result.withFingerprint) {
            validateUser(username, password, result.token);
            console.log("Successfully encrypted credentials.");
            console.log("Encrypted credentials: " + result.token);
        } else if (result.withBackup) {
            console.log("Authenticated with backup password");
        }
    }

    var errorCallback = function (error) {
        if (error === FingerprintAuth.ERRORS.FINGERPRINT_CANCELLED) {
            console.log("FingerprintAuth Dialog Cancelled!");
        } else {
            console.log("FingerprintAuth Error: " + error);
        }
    }

    FingerprintAuth.encrypt(encryptConfig, successCallback, errorCallback);
}

function decrypt(username, token) {
    var decryptConfig = {
        clientId: appName,
        username: username,
        token: token
    };

    function successCallback(result) {
        console.log("successCallback(): " + JSON.stringify(result));
        if (result.withFingerprint) {
            console.log("Successful biometric authentication.");
            if (result.password) {
                console.log("Successfully decrypted credential token.");
                console.log("password: " + result.password);
                validateUser(username, result.password, token);
            }
        } else if (result.withBackup) {
            console.log("Authenticated with backup password");
        }
    }

    var errorCallback = function (error) {
        if (error === FingerprintAuth.ERRORS.FINGERPRINT_CANCELLED) {
            console.log("FingerprintAuth Dialog Cancelled!");
        } else {
            console.log("FingerprintAuth Error: " + error);
        }
    }

    FingerprintAuth.decrypt(decryptConfig, successCallback, errorCallback);
}
